TARGET=magit

all:
	pandoc -t slidy -s $(TARGET).txt -o $(TARGET).html

pdf:
	pandoc -t beamer $(TARGET).txt -V theme:Copenhagen -V colortheme:orchid -o $(TARGET).pdf


clean:
	rm -f *~ $(TARGET).html $(TARGET).pdf
